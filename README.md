#AZD

Project Build/Starter Tool
![azd](http://www.northerntransmissions.com/wp-content/uploads/2017/03/17212134_1344946918903572_735227990886031276_o.png)

Concept
===

- Scaffold Project Boilerplate
- Browserify JavaScript Source Code
- Combine CSS Source Code
- Run a Local Express Server
- Pocky.js Included for Ease

Usage
===

Default Command Shows All Possible Commands

```
azd
```

Scaffold Out Project Boilerplate

```
azd scaffold
```

Demolish/Destroy Existing Project

```
azd demolish
```

Compile/Bundle Javascript Source

```
azd build-scripts
azd build-scripts -i src/source.js //custom entry file
azd build-scripts -o dist/bundle.js //custom output file
```

Compile/Combine CSS Source

```
azd build-styles
azd build-styles -i src/app.css //custom entry file
azd build-styles -o dist/all.css //custom output file
```

Run Local Express Server

```
azd serve
azd serve -p 8080 //custom server port
```


Example Project Tree

```
root
server.js
package.json
package-lock.json
README.md
|_ dist
    |_ index.html
    |_ main.css
    |_ main.js
|_ src
    |_ index.js
    |_ index.css
```

Example Source Script

```
const { Application, View } = require('pocky.js');
const { html } = require('pocky.js').util;

const app = new Application();
const $container = document.body;

class HomePage extends View {
    constructor(options){
        super(options);
    }

    render(){
        var bus = this.emitter,
            parent = this.parentNode;

        function onclick(){
            bus.emit('home:click', parent, 'you clicked me!');
        }

        return html`<div id=${this.id} class=${this.classList.join(' ')}>
            <main>
                <h1 onclick=${onclick}>Hello ${this.data.name}!</h1>
            </main>
        </div>`;
    }
}

const $home = new HomePage({
    id: 'home-page',
    classList: ['home', 'page', 'app'],
    parentNode: $container,
    emitter: app,
    data: { name: 'World' }
});

app.on('home:click', window.alert);
app.router.on({ '*': () => $home.data ? $home.mount() : setTimeout(() => $home.mount(), 500) });
```

Example Source Styles

```
@import '/node_modules/normalize-css/normalize.css';

body {
    background-color: mistyrose;
}

#home-page main {
    display: flex;
    width: 100vw;
    height: 100vh;
    justify-content: center;
    align-items: center;
    color: #fff;
}
```
