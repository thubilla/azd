const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());
app.set('port', process.argv[2]);
app.use(express.static('dist/'));

app.listen(app.get('port'), function(){
	console.log(`Express started server listening on ${app.get('port')}; press CTRL-C to terminate.`);
});