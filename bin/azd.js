#! /usr/bin/env node

const yargs = require('yargs');
const shell = require('shelljs');
const chalk = require('chalk');
const argv = yargs.usage('$0 command')
	.command('scaffold', 'scaffold app directories', function(yargs){
		var command = 'mkdir -p src && mkdir -p dist && cp bin/lib/index.html dist && cp bin/lib/server.js ./ && cp bin/lib/index.js src && cp bin/lib/index.css src && cp bin/lib/.eslintrc.json ./';

		console.log(chalk.blue(command));
		shell.exec(`${command}`);
	})
	.command('demolish', 'destroy app directories', function(yargs){
		var command = 'trash src && trash dist && trash server.js';
		
		console.log(chalk.blue(command));
		shell.exec(`${command}`);
	})
	.command('lint', 'lint all javascript source', function(yargs){
		var command = 'eslint ./**/*.js';

		console.log(chalk.blue(command));
		shell.exec(`npx ${command}`);
	})
	.command('markup', 'transform markdown files into html', function(yargs){
		var argv = yargs.reset()
			.options({
				'i': {
					alias: 'input',
					demandOption: true,
					describe: '.md file to transformed',
					type: 'string'
				},
				'o': {
					alias: 'output',
					demandOption: true,
					describe: '.html file to be output',
					type: 'string'
				}
			})
			.help('h')
			.alias('h', 'help')
			.argv;

		var command = `showdown makehtml -i ${argv.input} -o ${argv.output}`;
		console.log(chalk.blue(command));
		shell.exec(`npx ${command}`);	
	})
	.command('serve', 'serve static build', function(yargs){
		var argv = yargs.reset()
			.option('p', {
				alias: 'port',
				demandOptions: true,
				default: '3000',
				describe: 'server listens on port',
				type: 'string'
			})
			.help('h')
			.alias('h', 'help')
			.argv;

		var command = `nodemon server.js ${argv.port}`;
		console.log(chalk.blue(command));
		shell.exec(`npx ${command}`);
	})
	.demand(1, 'must provide a valid command')
	.help('h')
	.alias('h', 'help')
	.argv;
